#pragma once

typedef struct ode_problem
{
    double *solution;
    double *initial_vector;
    int vector_size;
    int solution_size;
    int steps_count;
    double start_time, time_step;
    double *(*fun)(double, double *, int);
} ODEproblem;
