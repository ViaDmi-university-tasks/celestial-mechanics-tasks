#include <stdlib.h>
#include "ODE-problem.h"

double *_get_step_rk4(double time, double *vector, ODEproblem *problem)
{
    double *tmp_vector = malloc(problem->vector_size * sizeof(double));
    double *k1 = problem->fun(time, vector, problem->vector_size);

    for (int i = 0; i < problem->vector_size; i++)
        tmp_vector[i] = vector[i] + problem->time_step * k1[i] / 2;
    double *k2 = problem->fun(time + problem->time_step / 2, tmp_vector, problem->vector_size);

    for (int i = 0; i < problem->vector_size; i++)
        tmp_vector[i] = vector[i] + problem->time_step * k2[i] / 2;
    double *k3 = problem->fun(time + problem->time_step / 2, tmp_vector, problem->vector_size);

    for (int i = 0; i < problem->vector_size; i++)
        tmp_vector[i] = vector[i] + problem->time_step * k3[i];
    double *k4 = problem->fun(time + problem->time_step, tmp_vector, problem->vector_size);

    for (int i = 0; i < problem->vector_size; i++)
        tmp_vector[i] = vector[i] + (k1[i] + 2 * k2[i] + 2 * k3[i] + k4[i]) * problem->time_step / 6.0;

    free(k1);
    free(k2);
    free(k3);
    free(k4);
    return tmp_vector;
}

void rk4(ODEproblem *problem)
{
    problem->solution_size = (problem->vector_size + 1) * (problem->steps_count + 1);

    double time;
    double *value_vector = problem->initial_vector;
    problem->solution = malloc((problem->solution_size) * sizeof(double));

    problem->solution[0] = problem->start_time;
    for (int j = 0; j < problem->vector_size; j++)
        problem->solution[1 + j] = value_vector[j];

    for (int i = 1; i < problem->steps_count + 1; i++)
    {
        time = problem->start_time + problem->time_step * i;
        value_vector = _get_step_rk4(time, value_vector, problem);

        problem->solution[i * (problem->vector_size + 1)] = time;
        for (int j = 0; j < problem->vector_size; j++)
            problem->solution[i * (problem->vector_size + 1) + 1 + j] = value_vector[j];
    }
    free(value_vector);
}