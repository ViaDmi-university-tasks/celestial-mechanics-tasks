#!/usr/bin/gnuplot
set terminal png size 1000, 500
set xrange[-0.4:0.4]
set yrange[-0.2:0.2]

system("mkdir -p images")
set output "images/orbit1.png"
plot 'result.dat' using 4:5 with lines
set output "images/orbit2.png"
plot 'result.dat' using 6:7 with lines
set output "images/orbit3.png"
plot 'result.dat' using 8:9 with lines
