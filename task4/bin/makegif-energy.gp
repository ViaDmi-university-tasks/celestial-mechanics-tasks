#!/usr/bin/gnuplot

fileName = 'result.dat'
nRows    = system(sprintf("wc -l %s | awk '{print $1}'",fileName))
gifDir   = 'tmp-images/'
set term pngcairo size 1080, 1080
set grid

system("rm -rf ".gifDir)
system("mkdir -p ".gifDir)
system("echo Plotting $(expr ".nRows." - 2) frames...")
do for [i=0:nRows-2] {
    frameName = sprintf("%s/frame%05d.png",gifDir,i)
    set output frameName
    set size 1,1
    set multiplot layout 2,1 scale 1,1
    set xrange[-0.4:0.4]
    set yrange[-0.2:0.2]
    set size 1,0.5
    set origin 0,0.5
    plot fileName using 4:5 with lines linestyle 1 title "orbit" ,\
         fileName every ::i::i using 4:5 with points pointtype 7 lc 1 ps 3 title "1",\
         fileName every ::i::i using 6:7 with points pointtype 7 lc 2 ps 3 title "2",\
         fileName every ::i::i using 8:9 with points pointtype 7 lc 3 ps 3 title "3"
    set xrange[0:1.75]
    set yrange[-0.0000001:0.0000001]
    set size 1,0.5
    set origin 0,0
    plot fileName every ::0::i using 1:3 with lines title "relative energy diff"
    unset multiplot
}

system("cd ".gifDir."; ffmpeg -r 25 -i 'frame%05d.png' video.mp4 ")
system("mkdir -p videos")
system("mv tmp-images/video.mp4 videos/energy.mp4")
