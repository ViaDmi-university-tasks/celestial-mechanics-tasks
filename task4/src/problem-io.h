#pragma once
#include "ODE-problem.h"

void read_data_flat_problem(char *file_name, ODEproblem *problem);
void print_solution_in_file(char *file_name, ODEproblem *problem,
                            double *energies, double *rel_energies, int print_step);