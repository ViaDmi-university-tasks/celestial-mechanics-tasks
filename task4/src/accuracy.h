#include "ODE-problem.h"

void estimate_accuracy(ODEproblem *problem, double **energies, double **rel_energies, int *energies_size);
