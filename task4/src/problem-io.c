#include <stdio.h>
#include <stdlib.h>
#include "ODE-problem.h"

void read_data_flat_problem(char *file_name, ODEproblem *problem)
{
    FILE *file;
    file = fopen(file_name, "r");
    if (file == NULL)
    {
        fprintf(stderr, "Can not open file %s. Aborted.\n", file_name);
        exit(EXIT_FAILURE);
    }

    if (fscanf(file, "# %i %lf\n", &problem->vector_size, &problem->start_time) != 2)
        printf("Read error, first line: not one value in line, result may be wrong.\n");

    problem->initial_vector = malloc(4 * (problem->vector_size) * sizeof(double));
    for (int i = 0; i < problem->vector_size; i++)
    {
        if (fscanf(file, "%lf %lf %lf %lf",
                   &(problem->initial_vector)[2 * i + 0],
                   &(problem->initial_vector)[2 * i + 1],
                   &(problem->initial_vector)[2 * i + 0 + 2 * (problem->vector_size)],
                   &(problem->initial_vector)[2 * i + 1 + 2 * (problem->vector_size)]) != 4)
            printf("Read error: not 4 values in line, result may be wrong.\n");
    }

    problem->vector_size *= 4;
    fclose(file);
}

void print_solution_in_file(char *file_name, ODEproblem *problem,
                            double *energies, double *rel_energies, int print_step)
{
    FILE *file;
    file = fopen(file_name, "w");
    int lines_count = problem->solution_size / (problem->vector_size + 1);
    fprintf(file, "# %i \n", lines_count);
    for (int i = 0; i < lines_count; i = i + print_step)
    {
        fprintf(file, "%.15e ", problem->solution[i * (problem->vector_size + 1)]);
        fprintf(file, "%.15e %.15e ", energies[i], rel_energies[i]);
        for (int j = 1; j < problem->vector_size + 1; j++)
            fprintf(file, "%.15e ", problem->solution[i * (problem->vector_size + 1) + j]);
        fprintf(file, "\n");
    }
    fclose(file);
}
