#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "ODE-problem.h"

double *_calculate_energy(ODEproblem *problem, double **energies, int *energies_size)
{
    double kinetic, potential, distance2;
    int line_size = problem->vector_size + 1;
    *energies_size = problem->solution_size / line_size;
    *energies = malloc((*energies_size) * sizeof(double));
    for (int i = 0; i < (*energies_size); i++)
    {
        kinetic = 0;
        potential = 0;
        for (int j = 0; j < problem->vector_size / 2; j = j + 2)
        {
            kinetic += pow(problem->solution[i * line_size + 1 + problem->vector_size / 2 + j + 0], 2);
            kinetic += pow(problem->solution[i * line_size + 1 + problem->vector_size / 2 + j + 1], 2);
            for (int k = j + 2; k < problem->vector_size / 2; k = k + 2)
            {
                distance2 = pow(problem->solution[i * line_size + 1 + k + 0] - problem->solution[i * line_size + 1 + j + 0], 2) +
                            pow(problem->solution[i * line_size + 1 + k + 1] - problem->solution[i * line_size + 1 + j + 1], 2);
                potential += 1.0 / sqrt(distance2);
            }
        }
        kinetic /= 6.0;
        potential /= -9.0;
        (*energies)[i] = kinetic + potential;
    }
}

void estimate_accuracy(ODEproblem *problem, double **energies, double **rel_energies, int *energies_size)
{
    _calculate_energy(problem, energies, energies_size);
    *rel_energies = malloc((*energies_size) * sizeof(double));
    for (int i = 0; i < (*energies_size); i++)
        (*rel_energies)[i] = ((*energies)[i] - (*energies)[0]) / (*energies)[0];
}
