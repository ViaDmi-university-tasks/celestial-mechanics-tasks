#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "problem-io.h"
#include "rk4.h"
#include "ODE-problem.h"
#include "estimate_period.h"
#include "accuracy.h"

double *motion_equations_flat(double time, double *value_vector, int vector_size)
{
    double delta_x, delta_y, distance;
    double *derivative_vector = malloc(vector_size * sizeof(double));

    for (int i = 0; i < vector_size / 2; i++)
        derivative_vector[i] = value_vector[i + vector_size / 2];

    for (int i = vector_size / 2; i < vector_size; i = i + 2)
    {
        derivative_vector[i + 0] = 0;
        derivative_vector[i + 1] = 0;

        for (int j = 0; j < vector_size / 2; j = j + 2)
        {
            if (j != i - vector_size / 2)
            {
                delta_x = value_vector[i - vector_size / 2 + 0] - value_vector[j + 0];
                delta_y = value_vector[i - vector_size / 2 + 1] - value_vector[j + 1];
                distance = pow(pow(delta_x, 2) + pow(delta_y, 2), 1.5);

                derivative_vector[i + 0] += delta_x / distance;
                derivative_vector[i + 1] += delta_y / distance;
            }
        }

        derivative_vector[i + 0] *= -1 / 3.0;
        derivative_vector[i + 1] *= -1 / 3.0;
    }
    return derivative_vector;
}

void check_argument_count(int argc, char *program_name)
{
    if (argc < 6)
    {
        fprintf(stderr, "Start error: not enough arguments.\n");
        fprintf(stderr, "Usage: %s [STEP] [STEPS_COUNT] [INPUT_FILE_NAME] [OUTPUT_FAILE_NAME] [PRINT_STEP]\n", program_name);
        exit(EXIT_FAILURE);
    }
    if (argc > 6)
    {
        printf("Warning: too many arguments.\n");
        printf("Only the first four arguments will be processed, others will be ignored.\n");
    }
}

int processing_print_step(char *argv)
{
    int print_step;
    sscanf(argv, "%i", &print_step);
    if (print_step < 1)
    {
        printf("Warning: print step is less than 1, using default value 1.");
        print_step = 1;
    }
    return print_step;
}

int main(int argc, char *argv[])
{
    check_argument_count(argc, argv[0]);
    char *innput_file_name = argv[3];
    char *output_file_name = argv[4];

    ODEproblem problem;
    sscanf(argv[1], "%lf", &problem.time_step);
    sscanf(argv[2], "%i", &problem.steps_count);
    problem.fun = &motion_equations_flat;
    read_data_flat_problem(innput_file_name, &problem);

    rk4(&problem);

    double *energies;
    double *rel_energies;
    int energies_size;
    estimate_accuracy(&problem, &energies, &rel_energies, &energies_size);

    double eps = pow(0.01, 2); // 0.000001
    double lower_est_period = 1.0;
    double upper_est_period = 2.0;
    estimate_period(eps, lower_est_period, upper_est_period, &problem);

    int print_step = processing_print_step(argv[5]);
    print_solution_in_file(output_file_name, &problem, energies, rel_energies, print_step);
    free(energies);
    free(rel_energies);
    free(problem.solution);
    return 0;
}
