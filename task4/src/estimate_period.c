#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "ODE-problem.h"

void _parsing_period(double lower_est_period, double upper_est_period,
                      double key, double *periods, int periods_size)
{
    int count = 0;
    double sum = 0;
    for (int i = 0; i < periods_size; i++)
    {
        if (periods[i] != key && periods[i] > lower_est_period && periods[i] < upper_est_period)
        {
            count += 1;
            sum += periods[i];
        }
    }

    double mean = 0.0;
    if (count < 1)
        printf("Warning: mean period could not be estimate.\n");
    else
        mean = sum / count;

    sum = 0;
    for (int i = 0; i < periods_size; i++)
    {
        if (periods[i] != key && periods[i] > lower_est_period && periods[i] < upper_est_period)
            sum += pow(periods[i] - mean, 2);
    }

    double std = 0.0;
    if (count < 2)
        printf("Warning: std period could not be estimate.\n");
    else
        std = sqrt(sum / (count - 1));

    printf("T = %.15e +- %.15e \n", mean, std);
}

void estimate_period(double eps, double lower_est_period, double upper_est_period, ODEproblem *problem)
{
    double diff;

    int periods_size = problem->solution_size / (problem->vector_size + 1);
    double *periods = malloc(periods_size * sizeof(double));
    double key = problem->start_time - 1;
    for (int i = 0; i < problem->solution_size / (problem->vector_size + 1); i++)
        periods[i] = key;

    for (int i = 1; i < problem->solution_size / (problem->vector_size + 1); i++)
    {
        diff = 0.0;
        for (int j = 1; j < problem->vector_size + 1; j++)
        {
            diff += pow(problem->solution[i * (problem->vector_size + 1) + j] -
                        problem->solution[0 * (problem->vector_size + 1) + j],2);
        }
        if (diff < eps)
            periods[i] = problem->solution[i * (problem->vector_size + 1)];
    }
    _parsing_period(lower_est_period, upper_est_period, key, periods, periods_size);
}
