#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "orbit-det.h"

void _go_next_line(FILE *file, int count)
{
    char tmp;
    for (int i = 0; i < count; i++)
    {
        do
            tmp = fgetc(file);
        while (tmp != '\n');
    }
}

double _read_dd_mm_ss_format(FILE *file)
{
    int sign = 1;
    double seconds;
    double radian_angle;
    int integer, minutes;
    double convert_const = M_PI / 180;
    if (fscanf(file, "%3i:%2i:%lf", &integer, &minutes, &seconds) < 3)
        printf("_read_dd_mm_ss_format: not all arguments were scanned. Result may be wrong.\n");
    if (integer < 0)
        sign = -1;
    radian_angle = (integer + sign * (minutes / 60.0 + seconds / 3600.0)) * convert_const;
    return radian_angle;
}

void _read_line(FILE *file, double *JD_day, double *RA, double *DEC, vector *coord)
{
    if (fscanf(file, "%lf %lf %lf %lf %lf %lf\n", JD_day, RA, DEC, &coord->x, &coord->y, &coord->z) < 6)
        printf("_read_line error: not all arguments were scanned. Result may be wrong.\n");

    *RA *= M_PI / 12.0;
    *DEC *= M_PI / 180.0;
}

void read_data(char *file_name, raw_data *data, vector *sun_coords, double *ecliptical_inclination)
{
    FILE *file;
    file = fopen(file_name, "r");
    if (file == NULL)
    {
        fprintf(stderr, "Can not open file %s. Aborted.\n", file_name);
        exit(EXIT_FAILURE);
    }

    _go_next_line(file, 1);
    for (int i = 0; i < 3; i++)
        _read_line(file, &data->JD_days[i], &data->RA[i], &data->DEC[i], &sun_coords[i]);

    _go_next_line(file, 1);
    *ecliptical_inclination = _read_dd_mm_ss_format(file);

    fclose(file);
}

void _write_angle_radian_format(FILE *file, orbit_model *orbit)
{
    fprintf(file, "%lf\n", orbit->inclination);
    fprintf(file, "%lf\n", orbit->periapsis_argument);
    fprintf(file, "%lf\n", orbit->node_longitude);
    fprintf(file, "%lf\n", orbit->initial_mean_anomaly);
}

void _write_angle_degree_format(FILE *file, orbit_model *orbit)
{
    double convert_const = 180.0 / M_PI;
    fprintf(file, "%lf\n", convert_const * orbit->inclination);
    fprintf(file, "%lf\n", convert_const * orbit->periapsis_argument);
    fprintf(file, "%lf\n", convert_const * orbit->node_longitude);
    fprintf(file, "%lf\n", convert_const * orbit->initial_mean_anomaly);
}

void write_orbit_elements_in_file(char *file_name, orbit_model *orbit, char *key)
{
    FILE *file;
    file = fopen(file_name, "w");
    if (file == NULL)
    {
        fprintf(stderr, "Can not open file %s. Aborted.\n", file_name);
        exit(EXIT_FAILURE);
    }

    const char *key1 = "rad";
    const char *key2 = "deg";

    fprintf(file, "# orbital elements (a, e, i, periapsis argument, ");
    fprintf(file, "node longitude, initial mean anomaly, JD_day)\n");
    fprintf(file, "%lf\n", orbit->semimajor_axis);
    fprintf(file, "%lf\n", orbit->eccentricity);

    if (strcmp(key, key1) == 0)
        _write_angle_radian_format(file, orbit);
    else if (strcmp(key, key2) == 0)
        _write_angle_degree_format(file, orbit);
    else
    {
        printf("write_coordinates_in_file: key %s incorrect or not given. Writing interrupted.\n", key);
        exit(EXIT_FAILURE);
    }

    fprintf(file, "%lf\n", orbit->JD_day);
    fclose(file);
}
