#include <math.h>
#include <stdio.h>
#include "vector.h"

double get_dot_product(vector *a, vector *b)
{
    double sum = a->x * b->x + a->y * b->y + a->z * b->z;
    return sum;
}

double get_vector_length(vector *a)
{
    double length = sqrt(get_dot_product(a, a));
    return length;
}

double get_angle(vector *a, vector *b)
{
    double a_length = get_vector_length(a);
    double b_length = get_vector_length(b);
    double dot_product = get_dot_product(a, b);

    double angle;
    if (a_length == 0.0 || b_length == 0.0)
    {
        printf("vector::get_angle:\n");
        printf("one of argument vectors length is zero, angle undefined; zero returned.\n");
        angle = 0.0;
    }
    else
        angle = acos(dot_product / (a_length * b_length));

    return angle;
}

vector scalar_multiply_vector(vector *a, double scalar)
{
    vector b;
    b.x = scalar * a->x;
    b.y = scalar * a->y;
    b.z = scalar * a->z;
    return b;
}

vector get_cross_product(vector *a, vector *b)
{
    vector c;
    c.x = a->y * b->z - a->z * b->y;
    c.y = a->z * b->x - a->x * b->z;
    c.z = a->x * b->y - a->y * b->x;
    return c;
}