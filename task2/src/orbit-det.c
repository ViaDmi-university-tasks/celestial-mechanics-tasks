#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "vector.h"
#include "orbit-det.h"

vector _get_minors_vector(vector *direction_cosine)
{
    vector minors_vector;
    minors_vector.x = direction_cosine[0].y * direction_cosine[2].z - direction_cosine[0].z * direction_cosine[2].y;
    minors_vector.y = direction_cosine[0].z * direction_cosine[2].x - direction_cosine[0].x * direction_cosine[2].z;
    minors_vector.z = direction_cosine[0].x * direction_cosine[2].y - direction_cosine[0].y * direction_cosine[2].x;
    return minors_vector;
}

void process_data(raw_data *observational_data, processed_data *data)
{
    data->time_interval1 = observational_data->JD_days[1] - observational_data->JD_days[0];
    data->time_interval2 = observational_data->JD_days[2] - observational_data->JD_days[1];
    data->full_time_interval = observational_data->JD_days[2] - observational_data->JD_days[0];

    data->relative_time1 = data->time_interval2 / data->full_time_interval;
    data->relative_time2 = data->time_interval1 / data->full_time_interval;

    double gravity_parameter = pow(2 * M_PI / 365.2422, 2);
    double constant = gravity_parameter * data->time_interval1 * data->time_interval2 / 6.0;
    data->coeff1 = constant * (1.0 + data->relative_time1);
    data->coeff2 = constant * (1.0 + data->relative_time2);

    for (int i = 0; i < 3; i++)
    {
        data->direction_cosine[i].x = cos(observational_data->DEC[i]) * cos(observational_data->RA[i]);
        data->direction_cosine[i].y = cos(observational_data->DEC[i]) * sin(observational_data->RA[i]);
        data->direction_cosine[i].z = sin(observational_data->DEC[i]);
    }

    data->minors_vector = _get_minors_vector(data->direction_cosine);
}

double _solve_distance_function(double P, double Q, double R, double C,
                                double initial_value, int max_iter, double eps)
{
    int counter = 0;
    double r, value;
    double previous_value;
    double numerator, deminator;

    value = initial_value;
    do
    {
        counter++;
        previous_value = value;
        r = sqrt(pow(previous_value, 2) + 2 * previous_value * C + pow(R, 2));

        numerator = previous_value - P + Q * pow(r, -3);
        deminator = 1 - 3 * Q * (previous_value + C) * pow(r, -5);
        value = previous_value - numerator / deminator;
        if (counter > max_iter)
        {
            printf("orbit-det::_solve_distance_function:\n");
            printf("maximum of iteration (%i) has been reached. Result may be wrong.\n", max_iter);
            printf("err: %lf\n", fabs(value - previous_value));
            break;
        }
    } while (fabs(value - previous_value) > eps);
    return value;
}

double _get_average_planet_distance(processed_data *data, vector *sun_coords, double initial_value,
                                    double *geocentrical_distance, double *geliocentric_distance)
{
    double U1, U2, U3, determinant;
    U1 = get_dot_product(&sun_coords[0], &data->minors_vector);
    U2 = get_dot_product(&sun_coords[1], &data->minors_vector);
    U3 = get_dot_product(&sun_coords[2], &data->minors_vector);
    determinant = get_dot_product(&data->direction_cosine[1], &data->minors_vector);

    double P = (U2 - data->relative_time1 * U1 - data->relative_time2 * U3) / determinant;
    double Q = (data->coeff1 * U1 + data->coeff2 * U3) / determinant;
    double R = get_vector_length(&sun_coords[1]);
    double C = -get_dot_product(&data->direction_cosine[1], &sun_coords[1]);

    *geocentrical_distance = _solve_distance_function(P, Q, R, C, initial_value, 100, 1e-5);
    *geliocentric_distance = sqrt(pow(*geocentrical_distance, 2) + 2 * C * (*geocentrical_distance) + pow(R, 2));
}

double *get_geocentric_distances(processed_data *data, vector *sun_coords, double initial_value)
{
    double geliocentrical_distance, averange_distance;
    _get_average_planet_distance(data, sun_coords, initial_value,
                                 &averange_distance, &geliocentrical_distance);

    double n1 = data->relative_time1 + data->coeff1 / pow(geliocentrical_distance, 3);
    double n2 = data->relative_time2 + data->coeff2 / pow(geliocentrical_distance, 3);

    double vector[2];
    double matrix[2][2];
    vector[0] = n1 * sun_coords[0].x - sun_coords[1].x + n2 * sun_coords[2].x;
    vector[0] += averange_distance * data->direction_cosine[1].x;
    vector[1] = n1 * sun_coords[0].y - sun_coords[1].y + n2 * sun_coords[2].y;
    vector[1] += averange_distance * data->direction_cosine[1].y;

    matrix[0][0] = n1 * data->direction_cosine[0].x;
    matrix[0][1] = n2 * data->direction_cosine[2].x;
    matrix[1][0] = n1 * data->direction_cosine[0].y;
    matrix[1][1] = n2 * data->direction_cosine[2].y;

    double determinant;
    double *distances = malloc(3 * sizeof(double));

    determinant = matrix[0][0] * matrix[1][1] - matrix[1][0] * matrix[0][1];
    distances[0] = (vector[0] * matrix[1][1] - vector[1] * matrix[0][1]) / determinant;
    distances[2] = (matrix[0][0] * vector[1] - matrix[1][0] * vector[0]) / determinant;
    distances[1] = averange_distance;

    if (distances[0] < 0 || distances[1] < 0 || distances[2] < 0)
    {
        printf("Error:orbit-det: at least one of distance is less than zero.");
        printf(" %lf %lf %lf\n", distances[0], distances[1], distances[2]);
        printf("Try another initial value for solve function.\n");
    }

    return distances;
}

vector *get_geliocentr_coords(double *distances, vector *sun_coords, vector *direction_cosine)
{
    vector *geliocentric_coords;
    geliocentric_coords = malloc(3 * sizeof(vector));
    for (int i = 0; i < 3; i++)
    {
        geliocentric_coords[i].x = distances[i] * direction_cosine[i].x - sun_coords[i].x;
        geliocentric_coords[i].y = distances[i] * direction_cosine[i].y - sun_coords[i].y;
        geliocentric_coords[i].z = distances[i] * direction_cosine[i].z - sun_coords[i].z;
    }
    return geliocentric_coords;
}

vector *get_elliptical_coords(vector *gelio_coords, double ecliptical_inclination)
{
    vector *elliptical_coords;
    elliptical_coords = malloc(3 * sizeof(vector));
    double sin_value = sin(-ecliptical_inclination);
    double cos_value = cos(-ecliptical_inclination);
    for (int i = 0; i < 3; i++)
    {
        elliptical_coords[i].x = gelio_coords[i].x;
        elliptical_coords[i].y = gelio_coords[i].y * cos_value - gelio_coords[i].z * sin_value;
        elliptical_coords[i].z = gelio_coords[i].y * sin_value + gelio_coords[i].z * cos_value;
    }
    return elliptical_coords;
}

double solve_sin_cos_system(double sin_value, double cos_value)
{
    double argument = atan2(sin_value, cos_value);
    if (argument < 0)
        argument += 2 * M_PI;

    return argument;
}

double get_area_ratio(double distance1, double distance2, double interval, double semiangle)
{
    double k = 2.0 * sqrt(distance1 * distance2) * cos(semiangle);
    double m = 4.0 / 3.0 * pow(interval, 2) / pow(k, 3);
    double l = ((distance1 + distance2) / k - 1.0) / 2;
    double area_ratio = 1 + m * (1.0 - 1.1 * m - 1.2 * l);
    return area_ratio;
}

double get_semilatus_rectum(double distance1, double distance2,
                            double semiangle, double time_interval, double area_ratio)
{
    double semilatus_rectum;
    double numerator, deminator;
    double constant = 2 * M_PI / 365.2422;
    numerator = area_ratio * distance1 * distance2 * sin(2 * semiangle);
    deminator = time_interval * constant;
    semilatus_rectum = pow(numerator / deminator, 2);
    return semilatus_rectum;
}

struct solution
{
    double eccentricity;
    double true_anomaly1;
    double true_anomaly2;
};

struct solution solve_system_eccentricity_true_anomaly(double distance1, double distance2,
                                                       double semilatus_rectum, double angle)
{
    struct solution solution;
    double term1 = semilatus_rectum / distance1 - 1.0;
    double term2 = term1 * cos(angle) / sin(angle) - (semilatus_rectum / distance2 - 1.0) / sin(angle);
    solution.eccentricity = sqrt(pow(term1, 2) + pow(term2, 2));

    double sin_true_anomaly = term2 / solution.eccentricity;
    double cos_true_anomaly = term1 / solution.eccentricity;
    solution.true_anomaly1 = solve_sin_cos_system(sin_true_anomaly, cos_true_anomaly);
    solution.true_anomaly2 = solution.true_anomaly1 + angle;

    return solution;
}

double get_semimajor_axis(double semilatus_rectum, double eccentricity)
{
    return semilatus_rectum / (1.0 - pow(eccentricity, 2));
}

double get_eccentric_anomaly(double true_anomaly, double eccentricity)
{
    double eccentric_anomaly;
    double factor = sqrt((1.0 - eccentricity) / (1.0 + eccentricity));

    if (true_anomaly < M_PI)
    {
        eccentric_anomaly = 2.0 * atan(factor * tan(true_anomaly / 2.0));
    }
    else if (true_anomaly > M_PI)
    {
        eccentric_anomaly = 2.0 * (M_PI + atan(factor * tan(true_anomaly / 2.0)));
    }
    else if (true_anomaly == M_PI)
        eccentric_anomaly = M_PI;

    return eccentric_anomaly;
}

double get_mean_anomaly(double eccentric_anomaly, double eccentricity)
{
    return eccentric_anomaly - eccentricity * sin(eccentric_anomaly);
}

double get_inclination(vector *coords)
{
    return acos(coords->z);
}

double get_node_longitude(vector *coords)
{
    return solve_sin_cos_system(coords->x, -coords->y);
}

double get_periapsis_argument(vector *coords, double inclination, double node_longitude, double true_anomaly)
{
    double length = get_vector_length(coords);
    double cos_latitude = (coords->x * cos(node_longitude) + coords->y * sin(node_longitude)) / length;
    double sin_latitude = coords->z / (length * sin(inclination));

    double latitude_argument;
    latitude_argument = solve_sin_cos_system(sin_latitude, cos_latitude);

    double periapsis_argument = latitude_argument - true_anomaly;
    if (periapsis_argument < 0)
        periapsis_argument += 2 * M_PI;
    return periapsis_argument;
}

void determinate_orbit_elements(orbit_model *orbit, processed_data *data,
                                vector *radius_vector1, vector *radius_vector2)
{
    double distance1 = get_vector_length(radius_vector1);
    double distance2 = get_vector_length(radius_vector2);
    double semiangle = get_angle(radius_vector1, radius_vector2) / 2.0;
    vector plane_vector = get_cross_product(radius_vector1, radius_vector2);
    plane_vector = scalar_multiply_vector(&plane_vector, 1.0 / get_vector_length(&plane_vector));

    double interval = 2 * M_PI / 365.2422 * data->full_time_interval;
    double area_ratio = get_area_ratio(distance1, distance2, interval, semiangle);

    double semilatus_rectum = get_semilatus_rectum(distance1, distance2, semiangle,
                                                   data->full_time_interval, area_ratio);
    struct solution solution = solve_system_eccentricity_true_anomaly(distance1, distance2,
                                                                      semilatus_rectum, 2 * semiangle);
    double eccentric_anomaly1 = get_eccentric_anomaly(solution.true_anomaly1, solution.eccentricity);
    double eccentric_anomaly2 = get_eccentric_anomaly(solution.true_anomaly2, solution.eccentricity);
    double eccentric_anomaly_difference = eccentric_anomaly2 - eccentric_anomaly1;
    double critical_angle = 8 / 180.0 * M_PI;
    if (eccentric_anomaly_difference > critical_angle)
        printf("orbit-det: eccentric anomaly difference (%lf) large enough, result may be wrong.\n", eccentric_anomaly_difference);

    orbit->eccentricity = solution.eccentricity;
    orbit->semimajor_axis = get_semimajor_axis(semilatus_rectum, solution.eccentricity);
    orbit->initial_mean_anomaly = get_mean_anomaly(eccentric_anomaly1, solution.eccentricity);

    orbit->inclination = get_inclination(&plane_vector);
    orbit->node_longitude = get_node_longitude(&plane_vector);
    orbit->periapsis_argument = get_periapsis_argument(radius_vector1, orbit->inclination,
                                                       orbit->node_longitude, solution.true_anomaly1);
}
