#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "vector.h"
#include "orbit-det.h"
#include "orbit-det-io.h"

void check_argument_count(int argc, char *program_name)
{
    if (argc < 4)
    {
        fprintf(stderr, "Start error: not enough arguments.\n");
        fprintf(stderr, "Usage: %s -[KEY] [INPUT_FILE_NAME] [OUTPUT_FAILE_NAME]\n", program_name);
        exit(EXIT_FAILURE);
    }
    if (argc > 4)
    {
        printf("Warning: too many arguments.\n");
        printf("Only the first two file names will be processed, others will be ignored.\n");
    }
}

char *key_processing(char *program_name, char *arg_key)
{
    char *key;
    if (arg_key[0] != '-')
    {
        fprintf(stderr, "Missing key.\n");
        fprintf(stderr, "Usage: %s -[KEY] [INPUT_FILE_NAME] [OUTPUT_FAILE_NAME]\n", program_name);
        exit(EXIT_FAILURE);
    }
    else if (arg_key[1] == 'r')
        key = "rad";
    else if (arg_key[1] == 'd')
        key = "deg";
    else
    {
        fprintf(stderr, "Incorrect key: %c. Correct keys:\n", arg_key[1]);
        fprintf(stderr, "-r    write coordinates in radians\n");
        fprintf(stderr, "-d    write coordinates in degrees\n");
        exit(EXIT_FAILURE);
    }
    return key;
}

int main(int argc, char *argv[])
{
    check_argument_count(argc, argv[0]);
    char *key = key_processing(argv[0], argv[1]);
    char *innput_file_name = argv[2];
    char *output_file_name = argv[3];

    raw_data observational_data;
    vector sun_coords[3];
    double ecliptical_inclination;
    read_data(innput_file_name, &observational_data, sun_coords, &ecliptical_inclination);

    processed_data data;
    process_data(&observational_data, &data);

    double *distances = get_geocentric_distances(&data, sun_coords, 3.0);
    vector *gelio_coords = get_geliocentr_coords(distances, sun_coords, data.direction_cosine);
    vector *elliptical_coords = get_elliptical_coords(gelio_coords, ecliptical_inclination);

    orbit_model orbit;
    orbit.JD_day = observational_data.JD_days[0];
    determinate_orbit_elements(&orbit, &data, &elliptical_coords[0], &elliptical_coords[2]);
    write_orbit_elements_in_file(output_file_name, &orbit, key);

    return 0;
}
