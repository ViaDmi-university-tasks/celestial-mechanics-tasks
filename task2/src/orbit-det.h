#pragma once
#include "vector.h"

typedef struct raw_data
{
    double JD_days[3];
    double RA[3];
    double DEC[3];
} raw_data;

typedef struct processed_data
{
    double time_interval1;
    double time_interval2;
    double full_time_interval;

    double relative_time1;
    double relative_time2;
    double coeff1;
    double coeff2;

    vector minors_vector;
    vector direction_cosine[3];
} processed_data;

typedef struct orbit_model
{
    double semimajor_axis;       // semimajor axis
    double eccentricity;         // eccentricity
    double inclination;          // inclination
    double periapsis_argument;   // argument of periapsis
    double node_longitude;       // longitude of the ascending node
    double initial_mean_anomaly; // initial mean anomaly at JD_day

    double JD_day; // Julian day
} orbit_model;

void process_data(raw_data *observational_data, processed_data *data);
double *get_geocentric_distances(processed_data *data, vector *sun_coords, double initial_value);
vector *get_geliocentr_coords(double *distance, vector *sun_coords, vector *direction_cosine);
vector *get_elliptical_coords(vector *gelio_coords, double ecliptical_inclination);
void determinate_orbit_elements(orbit_model *orbit, processed_data *data,
                                vector *radius_vector1, vector *radius_vector2);