#!/bin/bash

_program=orbit-det.out
_list='test.dat'

echo "Tests are running..."
for _file in ${_list}; do
    ./${_program} -d ${_file} ${_file%%.*}.res
done
echo "Tests are completed. Result are written in *.res files."