#!/usr/bin/env python3
mass_ratio = 10
mu = 1.0 / (1 + mass_ratio)
const = 4.4 - 0.1345 * (5 - 1)


def zero_velocity_surface(x, y, z):
    r1 = ((x + mu)**2 + y**2 + z**2)**(0.5)
    r2 = ((x - 1 + mu)**2 + y**2 + z**2)**(0.5)
    return x**2 + y**2 + 2 * ((1 - mu) / r1  + mu / r2 ) - const
