#!/usr/bin/env python3
import numpy as np
from skimage import measure
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
import plotly.figure_factory as ff
from surface import *


def get_surface(fun, grid_size, bound):
    xl = np.linspace(-bound, bound, grid_size)
    X, Y, Z = np.meshgrid(xl, xl, xl)
    F = fun(X, Y, Z)
    vertices, simplices, normals, values = measure.marching_cubes(F, 0, spacing=[np.diff(xl)[0]]*3)
    vertices -= bound
    return vertices, simplices, normals, values


def plotly_surface(vertices, simplices, title: str, file_name: str):
    fig = ff.create_trisurf(x=vertices[:, 1], y=vertices[:, 0], z=vertices[:, 2], simplices=simplices,
                            plot_edges=False, colormap=[(0.05, 0.05, 0.25), (0.05, 0.05, 0.75)],
                            show_colorbar=False, showbackground=False, title=title)
    fig.update_layout(
        scene=dict(
            xaxis_title='',
            yaxis_title='',
            zaxis_title='',
            xaxis=dict(showticklabels=False),
            yaxis=dict(showticklabels=False),
            zaxis=dict(showticklabels=False),
        ),
    )
    #fig['data'][0].update(opacity=0.75)
    fig.write_html(file_name)


def matplotlib_surface(vertices, simplices, title: str, file_name: str):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot_trisurf(vertices[:, 0], vertices[:, 1], vertices[:, 2], triangles=simplices,
                    linewidth=0.0, shade=True, color='blue', edgecolors='blue',
                    alpha=1, antialiased=False)
    ax.view_init(elev=65, azim=30)
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    plt.title(title)
    plt.savefig(file_name)


def main():
    title = f"Zero-velocity surface, C = {const}, mass ratio = {mass_ratio}"
    grid_size, bound = 100, 2
    print(f"Calculating surface...")
    vertices, simplices, normals, values = get_surface(zero_velocity_surface, grid_size, bound)
    print(f"Plotting surface by plotly...")
    plotly_surface(vertices, simplices, title, "surface_plotly.html")
    print(f"Plotting surface by matplotlib...")
    matplotlib_surface(vertices, simplices, title, "surface_matplotlib.pdf")


if __name__ == '__main__':
    main()
