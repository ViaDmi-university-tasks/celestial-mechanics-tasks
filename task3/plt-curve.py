#!/usr/bin/env python3
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from surface import *


def plot_cut(x, y, eq, iteration, labels, x_zero_flag=False):
    cut_fig = plt.figure(iteration)
    plt.grid(linestyle='--')
    plt.axis('equal')
    plt.xlabel(labels[0])
    plt.ylabel(labels[1])
    plt.title(f"Zero-velocity surface (" + labels + f" plane cut)\n $C = ${const}, mass ratio = {mass_ratio}")
    if not x_zero_flag:
        x1, x2 = -mu, 1 - mu
    else:
        x1, x2 = 0, 0
    plt.plot(x1, 0, marker=".", markersize=10)
    plt.plot(x2, 0, marker=".", markersize=10/(mass_ratio)**(1/3))
    plt.contour(x, y, eq, [0])
    return cut_fig


def plot_curves(file_name: str):
    delta = 0.025
    xrange = np.arange(-2.0, 2.0, delta)
    yrange = np.arange(-2.0, 2.0, delta)
    zrange = np.arange(-2.0, 2.0, delta)

    x, y = np.meshgrid(xrange, yrange)
    eq = zero_velocity_surface(x, y, 0)
    Z_cut = plot_cut(x, y, eq, 1, 'xy')

    y, z = np.meshgrid(yrange, zrange)
    eq = zero_velocity_surface(0, y, z)
    X_cut = plot_cut(y, z, eq, 2, 'yz', True)

    x, z = np.meshgrid(xrange, zrange)
    eq = zero_velocity_surface(x, 0, z)
    Y_cut = plot_cut(x, z, eq, 3, 'xz')

    pp = PdfPages(file_name)
    pp.savefig(Z_cut, transparent=True)
    pp.savefig(X_cut, transparent=True)
    pp.savefig(Y_cut, transparent=True)
    pp.close()


def main():
    print(f"Plotting curves...")
    plot_curves('curvesPy.pdf')


if __name__ == '__main__':
    main()
