#pragma once
#include <stdio.h>
#include "ephemeris.h"

void read_data(char *file_name, orbit_model *orbit, int *date_count, double **JD_days,
               vector **sun_coords, double **ecliptical_inclination_series);
void write_coordinates_in_file(char *file_name, int series_size, double *JD_days,
                               double *RA_series, double *DEC_series, char *key);
