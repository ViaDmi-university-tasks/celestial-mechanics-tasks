#include <math.h>
#include <stdio.h>
#include "ephemeris.h"

void convert_orbit_angles_radian(orbit_model *orbit)
{
    double convert_const = M_PI / 180.0;
    orbit->inclination          *= convert_const;
    orbit->periapsis_argument   *= convert_const;
    orbit->node_longitude       *= convert_const;
    orbit->initial_mean_anomaly *= convert_const;
}

double solve_kepler_equaption(double eccentricity, double mean_anomaly, int max_iter, double eps)
{
    int counter = 0;
    double eccentric_anomaly, previous_value;

    mean_anomaly = fmod(mean_anomaly, 2 * M_PI);
    eccentric_anomaly = mean_anomaly;
    previous_value = 2.0 * mean_anomaly + 10 * eps;
    do
    {
        counter++;
        previous_value = eccentric_anomaly;
        eccentric_anomaly = mean_anomaly + eccentricity * sin(eccentric_anomaly);
        if (counter > max_iter)
        {
            printf("ephemeris::solve_kepler_equaption:\n");
            printf("maximum of iteration has been reached. Result may be wrong.\n");
            printf("err: %lf\n", eccentric_anomaly - previous_value);
            break;
        }
    } while (fabs(eccentric_anomaly - previous_value) > eps);
    return eccentric_anomaly;
}

double get_true_anomaly(double eccentricity, double eccentric_anomaly)
{
    double true_anomaly;
    double factor = sqrt((1.0 + eccentricity) / (1.0 - eccentricity));
    if (eccentric_anomaly < M_PI)
    {
        true_anomaly = 2.0 * atan(factor * tan(eccentric_anomaly / 2.0));
    }
    else if (eccentric_anomaly > M_PI)
    {
        true_anomaly = 2.0 * (M_PI + atan(factor * tan(eccentric_anomaly / 2.0)));
    }
    else if (eccentric_anomaly == M_PI)
        true_anomaly = M_PI;

    return true_anomaly;
}

double get_distance(double semimajor_axis, double eccentricity, double eccentric_anomaly)
{
    return semimajor_axis * (1.0 - eccentricity * cos(eccentric_anomaly));
}

vector get_elliptical_coords(double distance, double true_anomaly, orbit_model *orbit)
{
    vector coords;
    double latitude_argument = orbit->periapsis_argument + true_anomaly;
    double sin_latitude    = sin(latitude_argument);
    double cos_latitude    = cos(latitude_argument);
    double sin_longitude   = sin(orbit->node_longitude);
    double cos_longitude   = cos(orbit->node_longitude);
    double sin_inclination = sin(orbit->inclination);
    double cos_inclination = cos(orbit->inclination);

    coords.x = distance * (cos_latitude * cos_longitude - sin_latitude * sin_longitude * cos_inclination);
    coords.y = distance * (cos_latitude * sin_longitude + sin_latitude * cos_longitude * cos_inclination);
    coords.z = distance * sin_latitude * sin_inclination;
    return coords;
}

vector get_equatorial_coords(vector elliptical_coords, double ecliptic_inclination)
{
    vector equatorial_coords;
    double sin_value = sin(ecliptic_inclination);
    double cos_value = cos(ecliptic_inclination);

    equatorial_coords.x = elliptical_coords.x;
    equatorial_coords.y = elliptical_coords.y * cos_value - elliptical_coords.z * sin_value;
    equatorial_coords.z = elliptical_coords.y * sin_value + elliptical_coords.z * cos_value;
    return equatorial_coords;
}

vector get_geocentric_coords(vector equatorial_coords, vector sun_coords)
{
    vector geocentric_coords;
    geocentric_coords.x = equatorial_coords.x + sun_coords.x;
    geocentric_coords.y = equatorial_coords.y + sun_coords.y;
    geocentric_coords.z = equatorial_coords.z + sun_coords.z;
    return geocentric_coords;
}

double _get_vector_length(vector coords)
{
    return sqrt(pow(coords.x, 2) + pow(coords.y, 2) + pow(coords.z, 2));
}

void get_celestial_coords(vector coords, double *RA, double *DEC)
{
    *DEC = asin(coords.z / _get_vector_length(coords));
    if (coords.x == 0)
    {
        if (coords.y > 0)
            *RA = M_PI / 2;
        else
            *RA = -M_PI / 2;
    }
    else
    {
        if (coords.x > 0)
            *RA = atan(coords.y / coords.x);
        else
            *RA = atan(coords.y / coords.x) + M_PI;
    }
    if (*RA < 0)
        *RA += 2 * M_PI;
}