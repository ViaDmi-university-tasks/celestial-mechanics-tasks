#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "ephemeris.h"
#include "ephemeris-io.h"

void check_argument_count(int argc, char *program_name)
{
    if (argc < 4)
    {
        fprintf(stderr, "Start error: not enough arguments.\n");
        fprintf(stderr, "Usage: %s -[KEY] [INPUT_FILE_NAME] [OUTPUT_FAILE_NAME]\n", program_name);
        exit(EXIT_FAILURE);
    }
    if (argc > 4)
    {
        printf("Warning: too many arguments.\n");
        printf("Only the first two file names will be processed, others will be ignored.\n");
    }
}

char *key_processing(char *program_name, char *arg_key)
{
    char *key;
    if (arg_key[0] != '-')
    {
        fprintf(stderr, "Missing key.\n");
        fprintf(stderr, "Usage: %s -[KEY] [INPUT_FILE_NAME] [OUTPUT_FAILE_NAME]\n", program_name);
        exit(EXIT_FAILURE);
    }
    else if (arg_key[1] == 'r')
        key = "rad";
    else if (arg_key[1] == 'd')
        key = "dd:mm:ss";
    else
    {
        fprintf(stderr, "Incorrect key: %c. Correct keys:\n", arg_key[1]);
        fprintf(stderr, "-r    write coordinates in radians\n");
        fprintf(stderr, "-d    write coordinates in degree foramt (hh:mm:ss or dd:mm:ss)\n");
        exit(EXIT_FAILURE);
    }
    return key;
}

int main(int argc, char *argv[])
{
    check_argument_count(argc, argv[0]);
    char *key = key_processing(argv[0], argv[1]);
    char *innput_file_name = argv[2];
    char *output_file_name = argv[3];

    int date_count;
    orbit_model orbit;
    vector *sun_coords;
    double *RA_series, *DEC_series, *JD_days, *ecliptical_inclination_series;
    read_data(innput_file_name, &orbit, &date_count, &JD_days, &sun_coords, &ecliptical_inclination_series);
    convert_orbit_angles_radian(&orbit);

    double distance, mean_anomaly, eccentric_anomaly, true_anomaly;
    vector elliptical_coords, equatorial_coords, geocentric_coords;
    double mean_motion = 2 * M_PI / 365.2421897 / pow(orbit.semimajor_axis, 1.5);

    RA_series = malloc(sizeof(double) * date_count);
    DEC_series = malloc(sizeof(double) * date_count);
    for (int i = 0; i < date_count; i++)
    {
        mean_anomaly      = orbit.initial_mean_anomaly + mean_motion * (JD_days[i] - orbit.JD_day);
        eccentric_anomaly = solve_kepler_equaption(orbit.eccentricity, mean_anomaly, 10, 1e-6);
        true_anomaly      = get_true_anomaly(orbit.eccentricity, eccentric_anomaly);
        distance          = get_distance(orbit.semimajor_axis, orbit.eccentricity, eccentric_anomaly);
        elliptical_coords = get_elliptical_coords(distance, true_anomaly, &orbit);
        equatorial_coords = get_equatorial_coords(elliptical_coords, ecliptical_inclination_series[i]);
        geocentric_coords = get_geocentric_coords(equatorial_coords, sun_coords[i]);
        get_celestial_coords(geocentric_coords, &RA_series[i], &DEC_series[i]);
    }

    write_coordinates_in_file(output_file_name, date_count, JD_days, RA_series, DEC_series, key);
    return 0;
}
