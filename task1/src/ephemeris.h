#pragma once

typedef struct orbit_model
{
    double semimajor_axis;       // semimajor axis
    double eccentricity;         // eccentricity
    double inclination;          // inclination
    double periapsis_argument;   // argument of periapsis
    double node_longitude;       // longitude of the ascending node
    double initial_mean_anomaly; // initial mean anomaly at JD_day

    double JD_day; // Julian day
} orbit_model;

typedef struct _vector_3d
{
    double x, y, z;
} vector;

void convert_orbit_angles_radian(orbit_model *orbit);
double solve_kepler_equaption(double eccentricity, double mean_anomaly, int max_iter, double eps);
double get_true_anomaly(double eccentricity, double eccentric_anomaly);
double get_distance(double semimajor_axis, double eccentricity, double eccentric_anomaly);
vector get_elliptical_coords(double distance, double true_anomaly, orbit_model *orbit);
vector get_equatorial_coords(vector elliptical_coords, double ecliptic_inclination);
vector get_geocentric_coords(vector equatorial_coords, vector sun_coords);
void get_celestial_coords(vector coords, double *RA, double *DEC);