# Построение эфемерид
Построение эфемерид малой планеты по известным параметрам ее орбиты.

- [Построение эфемерид](#построение-эфемерид)
  - [Сборка](#сборка)
  - [Использование](#использование)
    - [Запуск программы](#запуск-программы)
    - [Скрипт `run.sh`](#скрипт-runsh)

Алгоритм расчета эфемерид был реализован на языке C.

## Сборка
Сборка осуществляется с помощью `Makefile`.

Команды `Makefile`:
- `make`: сборка программы.
- `make exe`: запуск программы.
- `make clean`: удаляет файлы компиляции, программу.

## Использование
Программа читает данные из файла указанного при запуске файла (см. [далее](#запуск-программы)), пример такого файла: [bin/input.dat](bin/input.dat). Файл должен иметь структуру (комментарии обязательно однострочные, можно оставлять пустыми, например `# `.):
```
# orbital elements (a, e, i, periapsis argument, node longitude, initial mean anomaly, JD day)
<semimajor_axis, double, in astronomical unit>
<eccentricity, double>
<inclination, double, in degrees>
<argument of periapsis, double, in degrees>
<longitude of the ascending node, double, in degrees>
<initial mean anomaly, double, in degrees>
<JD day, double, in days>
# date count
<count of dates, integer, positive number>
# date X Y Z ecl. inc. (coordinates in astronomical units, angle in degrees)
<JD day, double> <X Sun coord> <Y Sun coord> <Z Sun coord> <ecliptic inclination
...
```

### Запуск программы
```console
$ ./ephemeris.out -[KEY] [INPUT_FILE_NAME] [OUTPUT_FAILE_NAME]
```
где `-[KEY]` может быть
- `-r`: записывает координаты в формате `радианы`.
- `-d`: записывает координаты в формате `часы:минуты:секунды` и `градусы:минуты:секунды`.

Другие аргументы:
- `[INPUT_FILE_NAME]`: название файла, из которого буду читаться данные.
- `[OUTPUT_FILE_NAME]`: название файла, в который будут записываться координаты.
Все последующие аргументы запуска будут проигнорированы.

Данные записываются в указанный при запуске файл, в первой строке которого указывается число строк `# <count of lines>`. Дальше следуют строки с Юлианской датой, прямым восхождением и склонение в одном из указанных выше форматов.

### Скрипт `run.sh`
bash-скрипт, запсукающий программу на тестовых данных [ceres.dat](bin/ceres.dat), [eros.dat](bin/eros.dat), [vesta.dat](bin/vesta.dat).